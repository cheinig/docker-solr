FROM solr:6.6.1-alpine

USER root
RUN apk add --no-cache tesseract-ocr zip
RUN cd /usr/share/tessdata && wget -O deu.traineddata https://github.com/tesseract-ocr/tessdata/blob/3.04.00/deu.traineddata?raw=true && wget -O eng.traineddata https://github.com/tesseract-ocr/tessdata/blob/3.04.00/eng.traineddata?raw=true
RUN mkdir /data
ADD org /data/org
RUN cd /data && zip /opt/solr/contrib/extraction/lib/tika-parsers-1.13.jar org/apache/tika/parser/ocr/TesseractOCRConfig.properties
USER solr